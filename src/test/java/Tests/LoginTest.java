package Tests;

import Pages.LoginPage;
import Pages.StartPage;
import com.codeborne.selenide.Configuration;
import org.testng.annotations.*;

import static com.codeborne.selenide.Selenide.clearBrowserCookies;
import static com.codeborne.selenide.Selenide.open;

public class LoginTest {
    LoginPage loginPage;

    @BeforeTest
    public void setUpBrowser() {
        Configuration.baseUrl = "https://lmslite47vr.demo.mirapolis.ru";
    }

    @BeforeMethod
    public void setUp() {
        open("/mira");
        loginPage = new LoginPage();
    }

    @DataProvider(name = "dataAuthorization")
    public Object[][] createDataAuthorization() {
        return new Object[][]{
                {"fominaelena", "1P73BP4Z", true},
                {"login", "pass", false},
                {"fominaelena", "", false},
                {"", "1P73BP4Z", false},
                {"", "", false}
        };
    }

    @Test(dataProvider = "dataAuthorization")
    public final void authorizationTest(String login, String password, boolean correct) {
        loginPage.fillLoginAndPass(login, password)
                .clickLoginButton();
        if (correct) {
            new StartPage().checkElments();
        } else {
            loginPage.erroreMessageIsPresent();
        }
    }

    @AfterMethod
    public void refreshBrowser() {
        clearBrowserCookies();
    }
}
