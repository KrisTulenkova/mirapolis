package Pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$x;

public class StartPage {
    private final SelenideElement startPage
            = $x("//*[@name='StudentStartPage']");

    public final void checkElments() {
        startPage.shouldBe(visible);
    }
}
