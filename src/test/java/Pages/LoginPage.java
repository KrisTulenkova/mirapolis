package Pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import static com.codeborne.selenide.Selenide.$x;

public class LoginPage {
    private final SelenideElement loginField
            = $x("//input[@name='user']");
    private final SelenideElement passField
            = $x("//input[@name='password']");
    private final SelenideElement loginButton
            = $x("//button[text()='Войти']");

    public final LoginPage fillLoginAndPass(final String login, final String password) {
        loginField.setValue(login);
        passField.setValue(password);
        return this;
    }

    public final StartPage clickLoginButton() {
        loginButton.click();
        return new StartPage();
    }

    public final LoginPage erroreMessageIsPresent() {
        ExpectedConditions.alertIsPresent();
        return this;
    }
}
